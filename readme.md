Sprocket CMS
=============


Helper Functions
----------------
(hasSections()) ? 'yes' : 'no';

Helper Class
----------------

	(Cms::hasSections()) ? 'yes' : 'no';
	(Sprocket\Cms\Helpers\Cms::hasSections()) ? 'yes' : 'no';

	(Cms::hasBlog()) ? 'yes' : 'no';
	(Cms::pagesMenuTitle());


Messages Helper
----------------
There is a messages helper method for retrieving messages from the language file.

	Cms::msg('messages.dashboard.welcome');
	Lang::get('cms::messages.dashboard.welcome');
	trans('cms::messages.dashboard.welcome');

User Command
----------------
There is a Artisan to create new users: `user:create`
It will prompt you for the appropriate details.

To create new commands:
`php artisan command:make UserCreate --path=workbench/sprocket/cms/src/sprocket/cms/commands --namespace=Sprocket\\Cms\\Commands`

Seeding
----------------
`php artisan db:seed --class="Sprocket\Cms\Seeds\DatabaseSeeder"`


Exceptions
----------------

	try {
		throw new ActionNotAllowedException('You are not allowed to do that');
	} catch (ActionNotAllowedException $e) {
		return $e->getMessage();
	}


----------------

----------------

----------------
