<?php namespace Sprocket\Cms\Pages\Exceptions;

class NonExistentPageException extends \Exception {};
