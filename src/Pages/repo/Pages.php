<?php namespace Sprocket\Cms\Pages\Repo;

use Sprocket\Cms\Pages\DB\Page;
use Sprocket\Cms\Base\Repository;

class Pages implements Repository {

	public function getFirst()
	{
		return Page::find(1);
	}

}
