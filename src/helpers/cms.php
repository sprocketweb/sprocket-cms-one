<?php namespace Sprocket\Cms\Helpers;

use \Config;
use \Carbon\Carbon;
use \Lang;

class Cms {

	public static function hasSections()
	{
		return Config::get('cms::has_sections',false);
	}

	public static function hasBlog()
	{
		return Config::get('cms::has_blog',false);
	}

	public static function hasMedia()
	{
		return Config::get('cms::has_media',false);
	}

	public static function pagesMenuTitle()
	{
		return Config::get('cms::page.menu.title');
	}

	public static function msg($key)
	{
		return Lang::get('cms::'.$key);
	}

	/**
	 * returns 'is' or 'are' based on item count
	 * @param  integer $count
	 * @return string
	 */
	public static function isAre($count = 1)
	{
		return ($count != 1) ? 'are' : 'is';
	}

	/**
	 * message of items count
	 * @param  string $item  the entities
	 * @param  int $count number of item/s
	 * @param  string $class css class
	 * @return string        the message
	 */
	public static function totals( $item, $count, $class = NULL )
	{
		$tmpl = is_null($class) ? '<p>' : "<p class=\"$class\">";
		$tmpl .= 'There %s <span class="badge badge-primary">%d</span> %s.</p>';

		$html = sprintf($tmpl,
			static::isAre($count),
			$count,
			str_plural($item,$count)
		);

		return $html;
	}

	/**
	 * redirects back to referrer
	 * @param  string  $page   default page
	 * @param  integer $status http code
	 * @return redirect
	 */
	public static function goBack( $page = '/', $status = 302 )
	{
		$back = Redirect::getUrlGenerator()->getRequest()->headers->get('referer');
		if( empty($back) ) $back = $page;
		return Redirect::to($back, $status, Request::header());
	}

	/**
	 * days since date
	 * @param  timestamp $date the date to test
	 * @return [type]       [description]
	 */
	public static function daysAgoStr($date)
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
	}

	/**
	 * days since date
	 * @param  timestamp $date the date to test
	 * @return [type]       [description]
	 */
	public static function daysAgo($date, $icon = true)
	{
		// <time title="{{ $page->updated_at }}" datetime="{{ $page->updated_at }}"><i class="fa fa-clock-o"></i>
		// {{ \Carbon\Carbon::createFromTimeStamp(strtotime($page->updated_at))->diffForHumans() }}</time>

		// $tmpl = '<time title="%s" datetime="%s"><i class="fa fa-clock-o"></i>%s</time>';
		$tmpl = '<time title="%s" datetime="%s">%s%s</time>';
		return sprintf($tmpl,
				$date,
				$date,
				$icon?'<i class="fa fa-clock-o"></i> ':'',
				Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans()
				);
	}

	/**
	 * opening pre tag
	 * @return string
	 */
	public static function pre($heading=null)
	{
		if (!is_null($heading)) echo "<h1>$heading</h1>\n";
		echo '<pre>';
	}


	public static function isNew($date)
	{
		$last_login = new \Carbon(\Auth::user()->last_login);
		$date = new \Carbon($date);
		return ($date->between($last_login, new \Carbon ));
		return $last_login->lt($date);
	}


	/*
	 * getDaysDifference()
	 * Used to get the difference of a given date time to date time today
	 * @param
	 *  datetime, $datetime, Given date time
	 *  string, $timezone, Given timezone
	 * @return
	 *  int, Days difference between the given date time and date time today
	 *  NULL,
	 */

	public static function getDaysDiff($datetime = '', $timezone = 'Pacific/Auckland')
	{
		if (date_parse($datetime)) {

			// $Carbon = new \Carbon\Carbon($date2);
			// $last_login = new \Carbon(\Auth::user()->last_login);
			$Carbon = new \Carbon(\Auth::user()->last_login);

			// return days difference from date today
			return $Carbon->diffInHours($Carbon->createFromTimestamp(strtotime($datetime), $timezone));
		}

		// if the given date time is invalid
		echo 'invalid date';
		return NULL;
	}

	/**
	 * returns money as a formatted string
	 * @param  integer $ammount
	 * @param  string $symbol  currency symbol
	 * @return string
	 */
	public static function money($ammount, $symbol = '$')
	{
		return $symbol . money_format('%i', $ammount);
	}

	/**
	 * test for local environment
	 * @param  string  $env name of environment to test
	 * @return boolean
	 */
	public static function isLocal($env = 'local')
	{
		return app()->environment() == $env;
	}

	/**
	 * test for production environment
	 * @param  string  $env environment name to test
	 * @return boolean
	 */
	public static function isProduction($env = 'production')
	{
		return app()->environment() == $env;
	}

	/**
	 * test if the app is in dev mode
	 * @return boolean
	 */
	public static function isDev()
	{
		$env = app()->environment();
		$local_names = ['local','dev','development'];
		$remote_names = ['production','remote'];

		return ( in_array($env, $local_names) OR ! in_array($env, $remote_names) ) ? true : false;
	}

}
