<?php namespace Sprocket\Cms\Helpers;

use \Session;
use \Carbon\Carbon;

class UserHelper {

	public static function loginMessage($user)
	{
		$prev_login = $user->last_login;
		if ( ! empty($prev_login))
		{
			$prev_login_human = Carbon::createFromFormat('Y-m-d H:i:s', $prev_login)->diffForHumans();

			$tmpl = 'Welcome. You last logged in <time datetime="%s" title="%s">%s</time>';
			$msg = sprintf( $tmpl, $prev_login, $prev_login, $prev_login_human);
			Session::flash('login_message', $msg);
		}
	}

	public static function updateLogin($user)
	{
		$user->last_login = Carbon::now();
		$user->save();
	}

}
