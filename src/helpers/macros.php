<?php

$this->app['form']->macro('actions',function($type='basic')
{
	// return include('cms::layouts.forms.basicactions');
	// return \View::include('cms::layouts.forms.basicactions');
	// return $this->app['view']->render('cms::layouts.forms.basicactions');
	// return 'form actions';
	/*
	<div class="form-actions clearfix">
	{{ HTML::link(Request::referrer(),'Cancel', array('class' => 'btn btn-large', 'accesskey' => '.')) }}
	{{ Form::submit('Save', array('class' => 'btn btn-large btn-success pull-right', 'accesskey' => 's')) }}
	</div>
	*/
});

// http://nielson.io/2014/02/handling-checkbox-input-in-laravel/
Form::macro('cms_check_box', function($name, $value, $checked = false, $label = false)
{
	$checked = ($checked)?'checked':'';
	$tmpl = '<input type="checkbox" name="%s" value="%s" %s>';
	$html = sprintf($tmpl, $name, $value, $checked );

	if($label) {
		$tmpl_label = '<label>%s %s</label>';
		$html = sprintf($tmpl_label, $html, $label);
	}
	$opposite = $checked ? 'false': 'true';
	$hidden = '<input type="hidden" value="'.$opposite.'">';
	return $hidden . $html;
});
