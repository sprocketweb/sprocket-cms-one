<?php

return array(

	'version'		=> '0.1.0 alpha',
	'has_sections'	=> false,
	'has_media'		=> false,

	'site'	=> [
		'tld'			=> 'cms.test1',
		'name'			=> 'Sweet Dog Farm'
	]

);
