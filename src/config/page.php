<?php

return [

	'menu' => [
		'title'		=> 'Editor'
	],

	'rollbacks'		=> false,
	'drafts'		=> false,
	'autosave'		=> false,
	'create_new'	=> false,

];
