<?php namespace Sprocket\Cms\Seeds;

use \DB;
use \Seeder;

class PagesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('pages')->delete();
		$now = new \DateTime;
		$pages = [
			['title' => 'Home Page', 'content' => 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'Products', 'content' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'About Us', 'content' => 'Sed posuere consectetur est at lobortis. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam id dolor id nibh ultricies vehicula ut id elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.', 'created_at' => $now, 'updated_at' => $now],
			['title' => 'Contact Us', 'content' => 'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna.', 'created_at' => $now, 'updated_at' => $now],
		];

		DB::table('pages')->insert($pages);

		$this->command->info('Pages table seeded!');
	}

}
