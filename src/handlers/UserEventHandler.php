<?php namespace Sprocket\Cms\Handlers;

use Sprocket\Cms\Helpers\UserHelper as User;
use \Input;
use \Log;
use \Request;

class UserEventHandler {

	public function subscribe($events)
	{
		$events->listen('user.dashboard', 	'Sprocket\Cms\Handlers\UserEventHandler@onDashboard');
		$events->listen('user.login', 		'Sprocket\Cms\Handlers\UserEventHandler@onLogin');
		$events->listen('user.logout', 		'Sprocket\Cms\Handlers\UserEventHandler@onLogout');
		$events->listen('user.badlogin', 	'Sprocket\Cms\Handlers\UserEventHandler@onBadLogin');
	}

	public function onLogin($user)
	{
		User::loginMessage($user);
		User::updateLogin($user);
		Log::info($user->email . ' logged in');
	}

	public function onLogout($user)
	{
		Log::info($user->email . ' logged out');
	}

	public function onDashboard()
	{
		Log::info('dashboard');
	}

	public function onBadLogin($email)
	{
		Log::warning('Bad Login', [
			'email'					=> $email,
			'IP'					=> Request::getClientIp(),
			'HTTP_TRUE_CLIENT_IP'	=> Request::server('HTTP_TRUE_CLIENT_IP')
		]);
	}

}
