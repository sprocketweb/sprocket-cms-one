<?php namespace Sprocket\Cms\Repo;

abstract class DbRepository {

	public function getById($id)
	{
		return $this->model->find($id);
	}

	public function getAll()
	{
		return $this->model->all();
	}

	public function delete($id)
	{
		return $this->model->findOrFail($id)->delete();
	}

	public function create($data = null)
	{
		return new $this->model($data);
	}

}
