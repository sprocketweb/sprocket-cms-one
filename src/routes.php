<?php

Route::filter('isSuper', function()
{
    if ( ! Auth::user()->super)
    {
        return Redirect::route('dashboard')->with('message','You don\'t have enough access do that, sorry.');;
    }
});

/* Auth */
Route::get('/login','Sprocket\Cms\AuthController@getLogin');
Route::post('/login','Sprocket\Cms\AuthController@postLogin');
Route::get('/logout','Sprocket\Cms\AuthController@showLogout');

Route::get('/dashboard', 'Sprocket\Cms\DashboardController@index');

Route::group(['prefix' => 'admin','before'=>'auth'], function()
{
	Route::get('/',function(){
		return Redirect::route('dashboard');
	});
	// return Cms::msg('messages.dashboard.welcome');
	// Route::resource('page','Sprocket\Cms\PageController');

	Route::get('/dashboard', ['uses'=>'Sprocket\Cms\DashboardController@index', 'as'=>'dashboard']);
});

