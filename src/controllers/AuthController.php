<?php namespace Sprocket\Cms;

use \Auth;
use \Event;
use \Input;
use \Redirect;
use \View;

class AuthController extends BaseController {

	/**
	 * Login form
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		return View::make('cms::auth.login');
	}

	/**
	 * Process login attempt
	 * @return mixed view or redirect
	 */
	public function postLogin()
	{
		$userdata = array(
			'email' 	=> Input::get('email'),
			'password'	=> Input::get('password')
		);
		if ( Auth::attempt($userdata) )
		{
			Event::fire('user.login', Auth::user());
			return Redirect::intended('/admin/dashboard');
		}
		Event::fire('user.badlogin', Input::get('email'));
		return Redirect::to('login')->with('error','There was an error with your log in.');
	}

	/**
	 * logs user out
	 * @return redirect login page
	 */
	public function showLogout()
	{
		Event::fire('user.logout',Auth::user());
		Auth::logout();

		if (isset($_GET['back'])) return Redirect::back();

		return Redirect::to('login')->with('message','You have been logged out.');
	}

}
