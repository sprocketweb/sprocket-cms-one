<?php namespace Sprocket\Cms;

use \Cms\Pages\Repo\Pages;
use \Cms\Base\BaseController;
use \View;
// \Illuminate\Routing\Controllers\Controller

class DashboardController extends BaseController {

	/**
	 * Pages Repository
	 *
	 * @var page
	 */
	protected $page;

	public function __construct(Pages $page)
	{
		$this->page = $page;
	}

	public function index()
	{
		// return $this->page->getFirst()->content;
		// $page = $this->page->getFirst();
		return View::make('cms::dashboard.index');
	}

}
