<?php namespace Sprocket\Cms;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
Use \Event;

class CmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		AliasLoader::getInstance()->alias('Cms', 'Sprocket\Cms\Helpers\Cms');

		include __DIR__.'/../../helpers/macros.php';

		// Event::subscribe(new \Sprocket\Cms\Handlers\UserEventHandler);
		// Event::subscribe(new Handlers\UserEventHandler);


		$this->package('sprocket/cms');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		include __DIR__.'/../../handlers.php';
		include __DIR__.'/../../routes.php';

		$this->app['user:create'] = $this->app->share(function()
		{
		    return new Commands\UserCommand();
		});

		$this->commands('user:create');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
