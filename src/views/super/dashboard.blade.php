@extends('admin.base')

@section('main')
<h1>Super Dashboard</h1>

<hr>
<h4>Back Up</h4>
<div class="btn-group">
{{ link_to_action('Admin\SuperController@getBackup','Backup',NULL,['class'=>'btn btn-info disabled']) }}
{{ link_to_action('Admin\SuperController@getBackup','S3',[$s3=1],['class'=>'btn btn-default disabled']) }}
</div>


@stop
