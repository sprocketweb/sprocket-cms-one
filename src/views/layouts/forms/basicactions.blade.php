<div class="form-actions clearfix">

<a href="{{ URL::previous() }}" class="btn btn-info btn-lg">
	<i class="fa fa-minus-circle"></i> Cancel
</a>

<button type="submit" class="btn btn-lg btn-success pull-right">
	<i class="fa fa-save"></i> Save
</button>

</div>
