<div class="navbar navbar-default navbar-fixed-top navbar-main" role="navigation">

<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div><!--navbar-header-->

<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav pull-right">
    @if(Auth::user()->super)
        <li>
            <a class="" href="/super"><i class="fa fa-info-circle"></i> Super</a>
        </li>
    @endif
    <li>
            <a class="" href="admin/help"><i class="fa fa-info-circle"></i> Help</a>
        </li>
        <li>
            <a class="" href="/logout"><i class="fa fa-power-off"></i> Logout</a>
        </li>
    </ul>

@include('cms::layouts.nav')

</div><!--nav-collapse-->

</div><!--navbar-->
