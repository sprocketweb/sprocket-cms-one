<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--[if lt IE 9]><script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script><![endif]-->
        <title>@yield('title')</title>

        @section('css')
            {{ HTML::style("packages/sprocket/cms/vendor/bootstrap/css/bootstrap.min.css") }}
            {{ HTML::style("packages/sprocket/cms/vendor/font-awesome/css/font-awesome.min.css") }}
            {{ HTML::style("packages/sprocket/cms/vendor/redactor/redactor/redactor.css") }}
            {{ HTML::style("packages/sprocket/cms/css/sitewide.css") }}
        @show
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
@include('cms::layouts.nav_wrapper')

        <div class="container content-wrap">
        @include('cms::layouts.messages')
            <div class="row">
                <div class="col-sm-12 content-main">
                    @yield('content')
                </div>
            </div><!--row-->
        </div><!--container-->

        @section('scripts')
            {{ HTML::script("packages/sprocket/cms/vendor/jquery/jquery-1.11.0.min.js") }}
            {{ HTML::script("packages/sprocket/cms/vendor/bootstrap/js/bootstrap.min.js") }}
        @show
    </body>
</html>
