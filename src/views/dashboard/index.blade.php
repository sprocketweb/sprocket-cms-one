@extends('pages::layouts.main')

@section('content')
<div class="span10">
@if((Session::has('login_message')))
<div class="alert alert-success">{{ Session::get('login_message') }}</div>
@endif
<h1>Dashboard</h1>
{{ Cms::msg('messages.dashboard.welcome') }}

@include('cms::dashboard.panels')

@stop

@section('js')
{{ HTML::script('/assets/admin/js/dashboard.index.js') }}
@stop
