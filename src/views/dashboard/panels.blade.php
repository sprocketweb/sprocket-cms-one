
<div class="row dashboard-panels">

<div class="col-lg-4 dashboard-panel">
<h4><a href="/admin/blog">
	<i class="icon-calendar"></i>
Blog Posts</a></h4>
<ul>
<li><a href="/admin/blog/edit/8"> New Blog</a></li>
<li><a href="/admin/blog/edit/7"> testing</a></li>
<li><a href="/admin/blog/edit/2"> Welcome</a></li>
<li><a href="/admin/blog/edit/1"> Chocolate Zucchini Cookies</a></li>
</ul>
</div>

<!-- editor -->
<div class="col-lg-4 dashboard-panel">

<h4><a href="/admin/editor">
	<i class="icon-file"></i>
	Pages
</a></h4>
<ul>
<li><a href="/admin/editor/edit/5"> Contact</a></li>
<li><a href="/admin/editor/edit/6"> About Us</a></li>
<li><a href="/admin/editor/edit/9"> Resources</a></li>
<li><a href="/admin/editor/edit/7"> Blog</a></li>
</ul>
<div class="panel-button-bar">
<div class="span1 pull-right"><a href="/admin/editor/create" class="btn" title="add a new page">
<i class="icon-pencil"></i><span><strong>New</strong></span></a></div>
<div class="span1 pull-right"><a href="/admin/editor" class="btn" title="view all pages">
<i class="icon-eye-open"></i><span><strong>View</strong></span></a></div>
</div>

</div>

<!-- news -->
<div class="col-lg-4 dashboard-panel">

<h4><a href="/admin/news">
	<i class="icon-pencil"></i>
	News</a></h4>
<ul>
<li><a href="/admin/news/edit/10"> Testing</a></li>
<li><a href="/admin/news/edit/9"> news story</a></li>
<li><a href="/admin/news/edit/2"> test</a></li>
</ul>

<div class="panel-button-bar">
<div class="span1 pull-right"><a href="/admin/news/create" class="btn" title="add a new news item">
<i class="icon-pencil"></i><span><strong>New</strong></span></a></div>
<div class="span1 pull-right"><a href="/admin/news" class="btn" title="view all news items">
<i class="icon-eye-open"></i><span><strong>View</strong></span></a></div>
</div>

</div>

</div><!--row-->
